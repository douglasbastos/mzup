from flask import Flask, render_template
from webargs import fields
from webargs.flaskparser import use_args

from mzup.services import mongodb

app = Flask(__name__)


options_args = {
    "team_id": fields.Integer(),
    "player_id": fields.Str()
}


@app.route('/')
@use_args(options_args)
def index(kwargs):
    players = mongodb.get_transfer_market_history(**kwargs)
    return render_template('index.html', players=list(players), args=kwargs)
