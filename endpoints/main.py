import operator
from copy import deepcopy

from flask import jsonify, Flask, request

from pymongo import MongoClient


mongo_client = MongoClient('mongodb+srv://mzup:ZR3RKNsfBQgeyqqU@cluster0-jvgss.mongodb.net/default?retryWrites=true')
db = mongo_client['mz']


class Wings():
    def __init__(self, row):
        self.row = row

    @property
    def goleiro(self):
        result = 0
        result += self.row['speed'] * 0
        result += self.row['stamina'] * 0.15
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0
        result += self.row['shooting'] * 0
        result += self.row['heading'] * 0
        result += self.row['keeping'] * 0.6
        result += self.row['ball_control'] * 0.05
        result += self.row['tackling'] * 0
        result += self.row['aerial_passing'] * 0.1
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    @property
    def ponta(self):
        result = 0
        result += self.row['speed'] * 0.2
        result += self.row['stamina'] * 0.2
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0
        result += self.row['shooting'] * 0
        result += self.row['heading'] * 0
        result += self.row['keeping'] * 0
        result += self.row['ball_control'] * 0.12
        result += self.row['tackling'] * 0.03
        result += self.row['aerial_passing'] * 0.35
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    @property
    def lateral(self):
        result = 0
        result += self.row['speed'] * 0.23
        result += self.row['stamina'] * 0.2
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0.02
        result += self.row['shooting'] * 0
        result += self.row['heading'] * 0
        result += self.row['keeping'] * 0
        result += self.row['ball_control'] * 0.06
        result += self.row['tackling'] * 0.32
        result += self.row['aerial_passing'] * 0.07
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    @property
    def zagueiro(self):
        result = 0
        result += self.row['speed'] * 0.21
        result += self.row['stamina'] * 0.2
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0.03
        result += self.row['shooting'] * 0
        result += self.row['heading'] * 0.0
        result += self.row['keeping'] * 0
        result += self.row['ball_control'] * 0.04
        result += self.row['tackling'] * 0.35
        result += self.row['aerial_passing'] * 0.07
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    @property
    def volante(self):
        result = 0
        result += self.row['speed'] * 0.05
        result += self.row['stamina'] * 0.2
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0.05
        result += self.row['shooting'] * 0
        result += self.row['heading'] * 0
        result += self.row['keeping'] * 0
        result += self.row['ball_control'] * 0.16
        result += self.row['tackling'] * 0.28
        result += self.row['aerial_passing'] * 0.16
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    @property
    def meia(self):
        result = 0
        result += self.row['speed'] * 0.05
        result += self.row['stamina'] * 0.2
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0.07
        result += self.row['shooting'] * 0.02
        result += self.row['heading'] * 0
        result += self.row['keeping'] * 0
        result += self.row['ball_control'] * 0.2
        result += self.row['tackling'] * 0.12
        result += self.row['aerial_passing'] * 0.24
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    @property
    def cabeceador(self):
        result = 0
        result += self.row['speed'] * 0.05
        result += self.row['stamina'] * 0.2
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0
        result += self.row['shooting'] * 0.25
        result += self.row['heading'] * 0.3
        result += self.row['keeping'] * 0
        result += self.row['ball_control'] * 0.1
        result += self.row['tackling'] * 0
        result += self.row['aerial_passing'] * 0
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    @property
    def atacante(self):
        result = 0
        result += self.row['speed'] * 0.2
        result += self.row['stamina'] * 0.2
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0
        result += self.row['shooting'] * 0.33
        result += self.row['heading'] * 0
        result += self.row['keeping'] * 0
        result += self.row['ball_control'] * 0.17
        result += self.row['tackling'] * 0
        result += self.row['aerial_passing'] * 0
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)


class Short():
    def __init__(self, row):
        self.row = row

    @property
    def goleiro(self):
        result = 0
        result += self.row['speed'] * 0
        result += self.row['stamina'] * 0.15
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0
        result += self.row['shooting'] * 0
        result += self.row['heading'] * 0
        result += self.row['keeping'] * 0.6
        result += self.row['ball_control'] * 0.05
        result += self.row['tackling'] * 0
        result += self.row['aerial_passing'] * 0.1
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    @property
    def lateral(self):
        result = 0
        result += self.row['speed'] * 0.23
        result += self.row['stamina'] * 0.2
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0.06
        result += self.row['shooting'] * 0
        result += self.row['heading'] * 0
        result += self.row['keeping'] * 0
        result += self.row['ball_control'] * 0.06
        result += self.row['tackling'] * 0.32
        result += self.row['aerial_passing'] * 0.03
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    @property
    def zagueiro(self):
        result = 0
        result += self.row['speed'] * 0.21
        result += self.row['stamina'] * 0.2
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0.07
        result += self.row['shooting'] * 0
        result += self.row['heading'] * 0
        result += self.row['keeping'] * 0
        result += self.row['ball_control'] * 0.04
        result += self.row['tackling'] * 0.36
        result += self.row['aerial_passing'] * 0.03
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    @property
    def volante(self):
        result = 0
        result += self.row['speed'] * 0.05
        result += self.row['stamina'] * 0.2
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0.15
        result += self.row['shooting'] * 0
        result += self.row['heading'] * 0
        result += self.row['keeping'] * 0
        result += self.row['ball_control'] * 0.16
        result += self.row['tackling'] * 0.28
        result += self.row['aerial_passing'] * 0.06
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    @property
    def meia(self):
        result = 0
        result += self.row['speed'] * 0.05
        result += self.row['stamina'] * 0.2
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0.2
        result += self.row['shooting'] * 0.09
        result += self.row['heading'] * 0
        result += self.row['keeping'] * 0
        result += self.row['ball_control'] * 0.2
        result += self.row['tackling'] * 0.12
        result += self.row['aerial_passing'] * 0.04
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    @property
    def atacante(self):
        result = 0
        result += self.row['speed'] * 0.2
        result += self.row['stamina'] * 0.2
        result += self.row['intelligence'] * 0.1
        result += self.row['passing'] * 0
        result += self.row['shooting'] * 0.33
        result += self.row['heading'] * 0
        result += self.row['keeping'] * 0
        result += self.row['ball_control'] * 0.17
        result += self.row['tackling'] * 0
        result += self.row['aerial_passing'] * 0
        result += self.row['set_plays'] * 0
        result += self.row['experience'] * 0.05
        result += self.row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)



def player_skill(request):
    params = dict(deepcopy(request.args))
    player_id = params.pop('player_id', None)

    if not player_id:
        return "Não enviou player_id", 422

    result = list(db.history_transfers.find({
        'player_id': player_id
    }).sort('deadline', -1).limit(1))

    if not result:
        return "Não encontramos o jogador na nossa base", 404

    player = result[0]
    player.pop('_id', None)
    player.pop('sold', None)
    player.pop('purchase_date', None)
    player.pop('purchase_value', None)
    player.pop('name_purchaser_club', None)
    player.pop('id_purchaser_club', None)
    player.pop('name_selling_club', None)
    player.pop('id_selling_club', None)
    player.pop('asking_price', None)
    player['form'] = 9

    wings = Wings(player)
    short = Short(player)

    player['positions_wings'] = {
        'goleiro': wings.goleiro,
        'lateral': wings.lateral,
        'zagueiro': wings.zagueiro,
        'volante': wings.volante,
        'meia': wings.meia,
        'ponta': wings.ponta,
        'cabeceador': wings.cabeceador,
        'atacante': wings.atacante,
    }

    player['positions_short'] = {
        'goleiro': short.goleiro,
        'lateral': short.lateral,
        'zagueiro': short.zagueiro,
        'volante': short.volante,
        'meia': short.meia,
        'atacante': short.atacante,
    }

    MAPPING_SKILL_TRANSLATE = {
        'name': 'Nome',
        'value': 'Valor',
        'age': 'Idade',
        'speed': 'Velocidade',
        'stamina': 'Resistência',
        'intelligence': 'Inteligência',
        'passing': 'Passe Curto',
        'shooting': 'Chute',
        'heading': 'Cabeceio',
        'keeping': 'Defesa a Gol',
        'ball_control': 'Controle de Bola',
        'tackling': 'Desarme',
        'aerial_passing': 'Passe Longo',
        'set_plays': 'Bola Parada',
        'experience': 'Experiência',
        'form': 'Forma',
        'total_balls': 'Total de bolas',
        'deadline': 'Última Atualização',
    }

    table = ''
    for k, v in MAPPING_SKILL_TRANSLATE.items():
        table += f'<tr><td>{v}</td><td>{player[k]}</td></tr>'

    maximizations = [MAPPING_SKILL_TRANSLATE[m] for m in player.get('maximizations', [])]
    maximizations = 'Sem informação' if not maximizations else maximizations

    table_positions_wings = f'<tr><th>Posição</td><td>Pontos</th></tr>'
    positions = sorted(player['positions_wings'].items(), key=operator.itemgetter(1), reverse=True)
    for k, v in positions:
        table_positions_wings += f'<tr><td>{k}</td><td>{v}</td></tr>'

    table_positions_short = f'<tr><th>Posição</td><td>Pontos</th></tr>'
    positions = sorted(player['positions_short'].items(), key=operator.itemgetter(1), reverse=True)
    for k, v in positions:
        table_positions_short += f'<tr><td>{k}</td><td>{v}</td></tr>'


    html = '''
        <html>
            <head>
                <title>Descobrindo habilidades - MZUp</title>
                <style>
                table, th, td {{
                    border: 1px solid black;
                }}
                table {{
                    font-size: 22px;
                }}
                </style>
            </head>

            <body>
                <table class="table table-striped table-responsive table-bordered">
                    <tbody>
                        {table}
                    </tbody>
                </table>
                <BR>
                Atributos Maximizados: {maximizations}

                <h4>Jogando pela laterais<h4>

                <table class="table table-striped table-responsive table-bordered">
                    <tbody>
                        {table_positions_wings}
                    </tbody>
                </table>

                <h4>Jogando de passe curto<h4>
                
                <table class="table table-striped table-responsive table-bordered">
                    <tbody>
                        {table_positions_short}
                    </tbody>
                </table>

            </body>
        </html>
        '''
    return html.format(table=table, maximizations=maximizations,
                       table_positions_wings=table_positions_wings,
                       table_positions_short=table_positions_short
                       )


app = Flask(__name__)


@app.route('/mzup-skill-player')
def index():
    return player_skill(request)
