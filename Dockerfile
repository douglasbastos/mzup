FROM python:3.7

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
RUN apt-get -y update
RUN apt-get install -y google-chrome-stable tzdata

# install chromedriver
RUN apt-get install -yqq unzip
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/2.42/chromedriver_linux64.zip
RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/

RUN rm -f /etc/localtime
RUN ln -s /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# APP
RUN mkdir /usr/src/mzup
WORKDIR /usr/src/mzup
COPY . /usr/src/mzup

ENV CHROME_PATH /usr/local/bin/chromedriver
# set display port to avoid crash
ENV DISPLAY=:99
