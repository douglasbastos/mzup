import asyncio

from telegram import ParseMode

from mzup.commands import collect_team_values
from mzup.config import SCHEMA_MESSAGE_MATCHES_INFO
from mzup.services.drivers import chrome_headless
from mzup.services.managerzone.get_fixtures import go_to_fixtures_page, get_fixtures
from mzup.services.managerzone.login import login
from mzup.services.telegram_send_message import send_message


def run():
    fixtures = get_fixtures(type='xml')

    with chrome_headless() as driver:
        login(driver)
        go_to_fixtures_page(driver)
        fixtures_2 = get_fixtures(type='browser', driver=driver)

    teams_to_evalute = set()
    for match_id, fixture in fixtures.items():
        teams_to_evalute.update([fixture['away_team_id'], fixture['home_team_id']])

    value_teams = asyncio.run(collect_team_values.run(team_ids=teams_to_evalute))
    for id, fixture in fixtures.items():
        try:
            category = fixtures_2[id]['category']
            info = dict(
                home_team_value=value_teams[fixture['home_team_id']][category],
                away_team_value=value_teams[fixture['away_team_id']][category]
            )

            msg = SCHEMA_MESSAGE_MATCHES_INFO.format(
                date=fixture['date'],
                category=fixtures_2[id]['category'],
                competition_name=fixtures_2[id]['competition_name'],
                competition_type=fixtures_2[id]['competition_type'],
                home_team_name=fixture['home_team_name'],
                away_team_name=fixture['away_team_name'],
                home_team_value='{:,}'.format(int(info['home_team_value'])),
                away_team_value='{:,}'.format(int(info['away_team_value']))
            )
            print(msg)
            # send_message(msg, parse_mode=ParseMode.MARKDOWN)
        except KeyError:
            # TODO: Melhorar
            continue


if __name__ == '__main__':
    run()
