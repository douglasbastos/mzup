import asyncio
from typing import Iterable
import xml.etree.ElementTree as ET

from aiohttp import ClientSession

from mzup.services.mz_xml import get_team_player_list


QUOTATION = {
    'R$': 1.0,
    'USD': 2.8270069629181496,
    'EUR': 3.4950004019250462,
    'SEK': 0.38082326373153486,
    'MM': 0.38082326373153486,
    'UYU': 0.09785748857367034,
    'GBP': 5.084943989341958,
    'DKK': 0.47040051781689,
    'NOK': 0.40841398148087643,
    'CHF': 2.234432152584903,
    'CAD': 2.1741162760866777,
    'AUD': 2.159262914011673,
    'ILS': 0.6456098850620722,
    'MXN': 0.2611533367039524,
    'ARS': 1.0070676004197459,
    'BOB': 0.3575930412394175,
    'PYG': 0.000498497652202178,
    'RUB': 0.10020602358448971,
    'PLN': 0.7436641672114007,
    'ISK': 0.03973129092248469,
    'BGL': 1.7926801284993117,
    'ZAR': 0.47120401578910415,
    'US$': 2.8270069629181496,
    'THB': 0.06504080692747033,
    'SIT': 0.014836874279493565,
    'SKK': 0.09500016957530269,
    'JPY': 0.02284939599792372,
    'INR': 0.0647399538481817,
    'MZ': 0.38082326373153486
}


def casting_attributes(player):
    player['age'] = int(player['age'])
    player['value'] = int(player['value'])
    player['salary'] = int(player['salary'])
    return player

def get_players_by_category(players: dict) -> dict:

    seniors, u18, u21, u23 = [], [], [], []
    for player in players.values():
        player = casting_attributes(player)
        if int(player['age']) <= 18:
            u18.append(player)
        elif int(player['age']) <= 21:
            u21.append(player)
        elif int(player['age']) <= 23:
            u23.append(player)
        else:
            seniors.append(player)

    return {
        'SENIORS': seniors + u18 + u21 + u23,
        'U23': u23 + u21 + u18,
        'U21': u21 + u18,
        'U18': u18
    }


def get_value_team(players: dict, currency: str) -> list:
    players = sorted(players, key=lambda k: k['value'], reverse=True)[:11]
    return sum(player['value'] * QUOTATION[currency] for player in players)


def get_best_players_by_category(category_players, currency):
    return {category: get_value_team(players, currency)
            for category, players in category_players.items()}

def get_players(response):
    element = ET.fromstring(response.decode())
    players = element.findall(".//Player")
    return {player.attrib['id']: player.attrib for player in players}

def get_team_currency(response):
    element = ET.fromstring(response.decode())
    return element.findall(".//TeamPlayers")[0].attrib['teamCurrency']


async def run(team_ids: Iterable):
    async with ClientSession() as session:
        tasks = []
        for team_id in team_ids:
            tasks.append(get_team_player_list(session, team_id))

        responses = await asyncio.gather(*tasks)


    teams_players = dict(zip(team_ids, responses))
    best_players_teams = {}
    for team_id, response in teams_players.items():
        team_players = get_players(response)
        currency_team = get_team_currency(response)
        category_players = get_players_by_category(team_players)
        best_players_teams[team_id] = get_best_players_by_category(category_players, currency_team)

    return best_players_teams


if __name__ == '__main__':
    haha = asyncio.run(run(team_ids=[214971]))
    hehe = 1
