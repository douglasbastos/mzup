import asyncio
from typing import Iterable

from aiohttp import ClientSession
from parsel import Selector

from mzup.config import MZ_URL_SCOUT_REPORT
from mzup.services.log import logger
from mzup.services.request import fetch

MAPPING_SKILL = {
    'Velocidade': 'speed',
    'Resistência': 'stamina',
    'Inteligência': 'intelligence',
    'Passe Curto': 'passing',
    'Chute': 'shooting',
    'Cabeceio': 'heading',
    'Defesa a Gol': 'keeping',
    'Controle de Bola': 'ball_control',
    'Desarme': 'tackling',
    'Passe Longo': 'aerial_passing',
    'Bola Parada': 'set_plays',
    'Speed': 'speed',
    'Stamina': 'stamina',
    'Play Intelligence': 'intelligence',
    'Passing': 'passing',
    'Shooting': 'shooting',
    'Heading': 'heading',
    'Keeping': 'keeping',
    'Ball Control': 'ball_control',
    'Tackling': 'tackling',
    'Aerial Passing': 'aerial_passing',
    'Set Plays': 'set_plays',
}



def count_stars(html):
    return html.count('fa fa-star fa-2x lit')


def parser_skill(names: list):
    try:
        return [MAPPING_SKILL[name] for name in names]
    except KeyError:
        return []


def parser_html(text: str):
    sel = Selector(text)
    potencial = sel.xpath("//dl/dd[1]//li[@class='blurred']//text()").extract()
    stars = sel.xpath("//div[@class='flex-grow-1']/span[@class='stars']").extract()
    return {
        'highest_potential': parser_skill(potencial[0:2]),
        'lowest_potential': parser_skill(potencial[2:4]),
        'highest_stars': count_stars(html=stars[0]),
        'lowest_stars': count_stars(html=stars[1]),
        'training_speed_stars': count_stars(html=stars[2])
    }


async def request_scout_report(session: ClientSession, player_id: int) -> tuple:
    url = MZ_URL_SCOUT_REPORT.format(player_id)
    logger.info(f'Getting player_id scout report: {player_id}')
    return await fetch(url, session=session)


async def get_collect_scout_report(player_ids: Iterable, session_mz_id: str):
    cookies = {'PHPSESSID': session_mz_id}
    async with ClientSession(cookies=cookies) as session:
        tasks = []
        player_ids = list(player_ids)
        for player_id in player_ids:
            tasks.append(request_scout_report(session, player_id))

        responses = await asyncio.gather(*tasks)

        players_result = {}
        for player_id, response in zip(player_ids, responses):
            body = response.decode()
            if body:
                players_result[player_id] = {'scout_report': parser_html(body)}

        return players_result
