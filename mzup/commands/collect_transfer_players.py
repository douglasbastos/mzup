import asyncio

from mzup.services import mongodb
from mzup.services.drivers import chrome_headless
from mzup.services.managerzone.get_transfers_players import \
    get_transfers_players
from mzup.services.managerzone.login import login


def run():
    with chrome_headless() as driver:
        login(driver)
        players = asyncio.run(get_transfers_players(driver))
        mongodb.players_transfers_market_add(players)


if __name__ == '__main__':
    run()
