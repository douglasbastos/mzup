import json
import re
import pandas as pd
from time import sleep
from typing import Iterable

import requests
from parsel import Selector

from mzup.config import MZ_USER, MZ_PASSWORD, KEY_SKILL_MAP
from mzup.services.calculate_positions import Short, Wings
from mzup.services.drivers import chrome_headless


def run(driver):
    driver.get('https://www.managerzone.com/')

    username = driver.find_element_by_id("login_username")
    password = driver.find_element_by_id("login_password")

    username.send_keys(MZ_USER)
    password.send_keys(MZ_PASSWORD)

    driver.find_element_by_css_selector(".floatLeft .buttonClassMiddle").click()
    sleep(5)

    #  JOGADORES
    driver.get('https://www.managerzone.com/?p=players')
    sel = Selector(text=driver.page_source)

    numbers = sel.xpath("//div[@id='players_container']//a[@class='subheader']/text()").extract()
    number = [number for number in numbers if number.strip('\n\t')]

    name = sel.xpath("//div[@id='players_container']//span[@class='player_name']/text()").extract()
    player_id = sel.xpath("//div[@id='players_container']//span[@class='player_id_span']/text()").extract()
    age = sel.xpath("//div[@id='players_container']//table[1]/tbody/tr[1]/td[1]/strong/text()").extract()
    country = sel.xpath("//div[@class='dg_playerview_info']/table[1]/tbody/tr[4]/td/img/@src").extract()
    value = sel.xpath("//div[@id='players_container']//table[1]/tbody/tr[last() - 3]/td/span[@class='bold']/text()").extract()
    salary = sel.xpath("//div[@id='players_container']//table[1]/tbody/tr[last() - 2]/td/span[@class='bold']/text()").extract()
    total_balls = sel.xpath("//div[@id='players_container']//table[1]/tbody/tr[last() - 1]/td/span[@class='bold']/text()").extract()
    speed = sel.xpath("//tr[1]/td[@class='skillval']/text()").extract()
    speed_percent = sel.xpath("//tr[1]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract()
    stamina = sel.xpath("//tr[2]/td[@class='skillval']/text()").extract()
    stamina_percent = sel.xpath("//tr[2]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract()
    intelligence = sel.xpath("//tr[3]/td[@class='skillval']/text()").extract()
    intelligence_percent = sel.xpath("//tr[3]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract()
    passing = sel.xpath("//tr[4]/td[@class='skillval']/text()").extract()
    passing_percent = sel.xpath("//tr[4]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract()
    shooting = sel.xpath("//tr[5]/td[@class='skillval']/text()").extract()
    shooting_percent = sel.xpath("//tr[5]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract()
    heading = sel.xpath("//tr[6]/td[@class='skillval']/text()").extract()
    heading_percent = sel.xpath("//tr[6]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract()
    keeping = sel.xpath("//tr[7]/td[@class='skillval']/text()").extract()
    keeping_percent = sel.xpath("//tr[7]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract()
    ball_control = sel.xpath("//tr[8]/td[@class='skillval']/text()").extract()
    ball_control_percent = sel.xpath("//tr[8]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract()
    tackling = sel.xpath("//tr[9]/td[@class='skillval']/text()").extract()
    tackling_percent = sel.xpath("//tr[9]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract()
    aerial_passing = sel.xpath("//tr[10]/td[@class='skillval']/text()").extract()
    aerial_passing_percent = sel.xpath("//tr[10]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract()
    set_plays = sel.xpath("//tr[11]/td[@class='skillval']/text()").extract()
    set_plays_percent = sel.xpath("//tr[11]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract()
    experience = sel.xpath("//tr[12]/td[@class='skillval']/text()").extract()
    form = sel.xpath("//tr[13]/td[@class='skillval']/text()").extract()

    index = 0
    while True:
        try:
            info = {'salary': salary[index]}
        except IndexError:
            info = {}
            # Juvenil não tem salário
            info = {'salary': '0'}

        try:
            info.update({
                'number': number[index],
                'name': name[index],
                'player_id': int(player_id[index]),
                'age': int(age[index]),
                'country': country[index],
                'value': value[index],
                'total_balls': int(total_balls[index]),
                'speed': speed[index],
                'speed_percent': speed_percent[index],
                'stamina': stamina[index],
                'stamina_percent': stamina_percent[index],
                'intelligence': intelligence[index],
                'intelligence_percent': intelligence_percent[index],
                'passing': passing[index],
                'passing_percent': passing_percent[index],
                'shooting': shooting[index],
                'shooting_percent': shooting_percent[index],
                'heading': heading[index],
                'heading_percent': heading_percent[index],
                'keeping': keeping[index],
                'keeping_percent': keeping_percent[index],
                'ball_control': ball_control[index],
                'ball_control_percent': ball_control_percent[index],
                'tackling': tackling[index],
                'tackling_percent': tackling_percent[index],
                'aerial_passing': aerial_passing[index],
                'aerial_passing_percent': aerial_passing_percent[index],
                'set_plays': set_plays[index],
                'set_plays_percent': set_plays_percent[index],
                'experience': experience[index],
                'form': form[index],
            })
            yield info
        except IndexError:
            # Não existem mais jogadores
            break
        else:
            index += 1


def parser(row: dict):
    def get_number(value: str) -> int:
        try:
            return int(re.sub('[^0-9]', '', value))
        except ValueError:
            haha = 1

    def get_percent(value: str) -> float:
        if value == 'width: 2px;':
            return 0.25
        elif value == 'width: 4px;':
            return 0.5
        elif value == 'width: 6px;':
            return 0.75
        else:
            return 0.0

    row['country'] = row['country'][-6:-4]
    row['number'] = get_number(row['number'])
    row['salary'] = get_number(row['salary'])
    row['value'] = get_number(row['value'])
    row['experience'] = get_number(row['experience'])
    row['form'] = get_number(row['form'])

    row['speed'] = get_number(row['speed']) + get_percent(row.pop('speed_percent'))
    row['stamina'] = get_number(row['stamina']) + get_percent(row.pop('stamina_percent'))
    row['intelligence'] = get_number(row['intelligence']) + get_percent(row.pop('intelligence_percent'))
    row['passing'] = get_number(row['passing']) + get_percent(row.pop('passing_percent'))
    row['shooting'] = get_number(row['shooting']) + get_percent(row.pop('shooting_percent'))
    row['heading'] = get_number(row['heading']) + get_percent(row.pop('heading_percent'))
    row['keeping'] = get_number(row['keeping']) + get_percent(row.pop('keeping_percent'))
    row['ball_control'] = get_number(row['ball_control']) + get_percent(row.pop('ball_control_percent'))
    row['tackling'] = get_number(row['tackling']) + get_percent(row.pop('tackling_percent'))
    row['aerial_passing'] = get_number(row['aerial_passing']) + get_percent(row.pop('aerial_passing_percent'))
    row['set_plays'] = get_number(row['set_plays']) + get_percent(row.pop('set_plays_percent'))
    return row


def parser_player_maximization(body: str) -> Iterable:
    for line in body.split('var'):
        text_match = ' series = '
        if line.startswith(text_match):
            data = json.loads(line.replace(text_match, '').replace(';', ''))
            break
    else:
        return []

    for item in data:
        if item.get('showInNavigator') == 'false' and item['color'] == 'rgba(255,0,0,0.7)':
            code = item['data'][0]['y']
            yield KEY_SKILL_MAP[code]


def get_player_maximizations(player_id: int, session_id: str) -> list:
    url = f'https://www.managerzone.com/ajax.php?p=trainingGraph&sub=getJsonTrainingHistory&sport=soccer&player_id={player_id}'
    response = requests.get(url, cookies={'PHPSESSID': session_id})
    result = parser_player_maximization(body=response.content.decode())
    return list(result)
#
# def get_players_in_db():
#     result = db['players'].find()
#     return {player['player_id']: player for player in result}
#
# def get_diff_skill_players(old_player, new_player):
#     if not old_player:
#         send_message(f"Novo jogador {new_player['name']}")
#         return
#
#     for key, item in new_player.items():
#         if key == 'maximizations':
#             maxed = set(set(new_player['maximizations'])).difference(old_player['maximizations'])
#             if maxed:
#                 send_message(f"Jogador {new_player['name']}: Maximizou em {maxed}")
#                 continue
#
#         if item != old_player[key]:
#             send_message(f"{new_player['number']} - {new_player['name']}: Mudou {key} "
#                   f"de {old_player[key]} para {item}")


if __name__ == '__main__':
    session_id = None
    # players_in_db = get_players_in_db()

    with chrome_headless() as driver:
        players = run(driver)
        results = []
        for player in players:
            # if not session_id:
            #     session_id = mz.get_session_id(driver)

            player_parsed = parser(player)
            print(f'Coletando {player_parsed["name"]}')
            player_id = player_parsed['player_id']

            # maximizations = get_player_maximizations(player_id, session_id)
            # player_parsed['maximizations'] = maximizations
            # send_message()(f'Player {player_parsed["number"]} is maxed in {maximizations}')

            # get_diff_skill_players(players_in_db.get(player_id), player)
            results.append(player_parsed)
            # db['players'].update(
            #     {'player_id': player_id},
            #     player_parsed,
            #     upsert=True'
            # )

        short = Short()
        wings = Wings()

        players_df = pd.DataFrame(results)
        players_df['goleiro'] = players_df.apply(lambda row: wings.goleiro(row), axis=1)
        players_df['lateral'] = players_df.apply(lambda row: wings.lateral(row), axis=1)
        players_df['zagueiro'] = players_df.apply(lambda row: wings.zagueiro(row), axis=1)
        players_df['volante'] = players_df.apply(lambda row: wings.volante(row), axis=1)
        players_df['meia'] = players_df.apply(lambda row: wings.meia(row), axis=1)
        players_df['ponta'] = players_df.apply(lambda row: wings.ponta(row), axis=1)
        players_df['cabeceador'] = players_df.apply(lambda row: wings.cabeceador(row), axis=1)
        players_df['atacante'] = players_df.apply(lambda row: wings.atacante(row), axis=1)

        players_df['lateral PC'] = players_df.apply(lambda row: short.lateral(row), axis=1)
        players_df['zagueiro PC'] = players_df.apply(lambda row: short.zagueiro(row), axis=1)
        players_df['volante PC'] = players_df.apply(lambda row: short.volante(row), axis=1)
        players_df['meia PC'] = players_df.apply(lambda row: short.meia(row), axis=1)

        players_df.to_csv('players.csv')

        haha = 1