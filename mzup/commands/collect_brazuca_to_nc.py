import asyncio
import csv

import json
import re

from aiohttp import ClientSession
from parsel import Selector

from mzup.services.request import fetch, fetch_post, fetch2

PHPSESSID = '22rv4q9vpftiedc5i7g4rl68r0'

countries = ["BR", "TN", "AL", "DZ", "AD", "AO", "AR", "AU", "AT", "AZ", "BD", "BY", "BE",
             "BO", "BA", "BG", "CA", "CL", "CN", "CO", "CR", "HR", "CY",
             "CZ", "DK", "DO", "EC", "EG", "SV", "EN", "EE", "FO", "FI", "FR",
             "GE", "DE", "GR", "GT", "HN", "HU", "IS", "IN", "ID", "IR", "IL",
             "IT", "JO", "KZ", "KE", "KW", "KG", "LV", "LB", "LI", "LT", "LU",
             "MK", "MY", "MT", "MX", "MD", "YU", "MA", "DC", "NL", "NG", "IX",
             "NO", "PK", "PA", "PY", "PE", "PH", "PL", "PT", "IE", "RO", "RU",
             "SA", "SX", "SN", "YX", "SG", "SK", "SI", "ZA", "KR", "ES", "SE",
             "CH", "TH", "TT", "TR", "UA", "AE", "US", "UY", "VE", "VN",
             "WA", "AD", "AO", "BG"]


async def get_leagues_in_country(country, sema):
    async with ClientSession() as session:
        url = f'https://mzlive.eu/mzlive.php?action=list&type=leagues&country={country}'
        response = await fetch(url, session=session, sema=sema)
        data = json.loads(response.decode())
        return (i['name'] for i in data)


async def get_team_ids(session, league, country, sema):
    print(f'Getting team in League: {league} - {country}')
    url = f'https://mzlive.eu/mzlive.php?currency=R%24&country={country}&league_name={league}&season=71'
    response = await fetch(url, session=session, sema=sema)
    data = json.loads(response.decode())
    try:
        return [i['team_id'] for i in data['standings'] if i['u21value'] > 5000000 and i['value'] > 5000000]
    except KeyError:
        return []

async def get_players_brazucas(session, team_id, sema):
    print(f'Getting brazucas in team_id: {team_id}')
    url = f'https://www.managerzone.com/ajax.php?p=players&sub=team_players&tid={team_id}&sport=soccer'
    data = {
        'filter_tab': 'summary',
        'age_from': 21,
        'age_to': 21,
        'origin_country': 1,
    }

    response = await fetch_post(url, session=session, data=data, sema=sema)
    result = json.loads(response.decode())
    return result['player_ids']


async def run():
    cookies = {'PHPSESSID': PHPSESSID}
    sema = asyncio.Semaphore(30)

    for country in countries:
        leagues = await get_leagues_in_country(country, sema)

        async with ClientSession() as session:
            tasks_get_team_ids = []
            for league in leagues:
                tasks_get_team_ids.append(get_team_ids(session, league, country, sema))

            responses = await asyncio.gather(*tasks_get_team_ids)
            team_ids = []
            for item in responses:
                team_ids += item

        async with ClientSession(cookies=cookies) as session:
            tasks = []
            for team_id in team_ids:
                tasks.append(get_players_brazucas(session, team_id, sema))

            responses = await asyncio.gather(*tasks)
            with open(f'players/players_u21_{country}.txt', 'a+') as txt:
                for player_ids in responses:
                    for player_id in player_ids:
                        txt.write(f'{player_id}\n')


def parse_skill(sel):
    def get_number(value: str) -> int:
        try:
            return int(re.sub('[^0-9]', '', value))
        except ValueError:
            return

    def get_percent(value: str) -> float:
        if value == 'width: 2px;':
            return 0.25
        elif value == 'width: 4px;':
            return 0.5
        elif value == 'width: 6px;':
            return 0.75
        else:
            return 0.0

    name = sel.xpath("//span[@class='player_name']/text()").extract_first()
    player_id = sel.xpath("//span[@class='player_id_span']/text()").extract_first()
    age = sel.xpath('//tr[1]/td[1]/strong/text()').extract_first()
    value = sel.xpath('//tr[5]/td/span[@class="bold"]/text()').extract_first()
    total_balls = sel.xpath('//tr[7]/td/span[@class="bold"]/text()').extract_first()
    speed = sel.xpath("//tr[1]/td[@class='skillval']/text()").extract_first()
    stamina = sel.xpath("//tr[2]/td[@class='skillval']/text()").extract_first()
    intelligence = sel.xpath("//tr[3]/td[@class='skillval']/text()").extract_first()
    passing = sel.xpath("//tr[4]/td[@class='skillval']/text()").extract_first()
    shooting = sel.xpath("//tr[5]/td[@class='skillval']/text()").extract_first()
    heading = sel.xpath("//tr[6]/td[@class='skillval']/text()").extract_first()
    keeping = sel.xpath("//tr[7]/td[@class='skillval']/text()").extract_first()
    ball_control = sel.xpath("//tr[8]/td[@class='skillval']/text()").extract_first()
    tackling = sel.xpath("//tr[9]/td[@class='skillval']/text()").extract_first()
    aerial_passing = sel.xpath("//tr[10]/td[@class='skillval']/text()").extract_first()
    set_plays = sel.xpath("//tr[11]/td[@class='skillval']/text()").extract_first()
    experience = sel.xpath("//tr[12]/td[@class='skillval']/text()").extract_first()
    form = sel.xpath("//tr[13]/td[@class='skillval']/text()").extract_first()
    speed_percent = sel.xpath(
        "//tr[1]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract_first()
    stamina_percent = sel.xpath(
        "//tr[2]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract_first()
    intelligence_percent = sel.xpath(
        "//tr[3]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract_first()
    passing_percent = sel.xpath(
        "//tr[4]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract_first()
    shooting_percent = sel.xpath(
        "//tr[5]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract_first()
    heading_percent = sel.xpath(
        "//tr[6]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract_first()
    keeping_percent = sel.xpath(
        "//tr[7]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract_first()
    ball_control_percent = sel.xpath(
        "//tr[8]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract_first()
    tackling_percent = sel.xpath(
        "//tr[9]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract_first()
    aerial_passing_percent = sel.xpath(
        "//tr[10]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract_first()
    set_plays_percent = sel.xpath(
        "//tr[11]/td[@class='skill_exact']//div[@class='skill_exact_bar']//@style").extract_first()

    print(f'Parseando jogador {player_id}')

    return {
        'name': name,
        'player_id': player_id,
        'speed': get_number(speed) + get_percent(speed_percent),
        'stamina': get_number(stamina) + get_percent(stamina_percent),
        'intelligence': get_number(intelligence) + get_percent(intelligence_percent),
        'passing': get_number(passing) + get_percent(passing_percent),
        'shooting': get_number(shooting) + get_percent(shooting_percent),
        'heading': get_number(heading) + get_percent(heading_percent),
        'keeping': get_number(keeping) + get_percent(keeping_percent),
        'ball_control': get_number(ball_control) + get_percent(ball_control_percent),
        'tackling': get_number(tackling) + get_percent(tackling_percent),
        'aerial_passing': get_number(aerial_passing) + get_percent(aerial_passing_percent),
        'set_plays': get_number(set_plays) + get_percent(set_plays_percent),
        'experience': get_number(experience),
        'form': get_number(form),
        'age': age,
        'total_balls': total_balls,
        'value': get_number(value),
    }

async def get_skill_player_brazuca(session, player_id, sema):
    print(f'Getting player: {player_id}')
    url = f'https://www.managerzone.com/ajax.php?p=nationalTeams&sub=search&ntid=104&type=national_team&sport=soccer'
    data = {
        'pid': player_id,
        'issearch': 1,
    }

    response = await fetch_post(url, session=session, data=data, sema=sema)
    body = response.decode()
    sel = Selector(text=body)
    return parse_skill(sel)



async def run_stage2():
    cookies = {'PHPSESSID': PHPSESSID}
    sema = asyncio.Semaphore(20)
    tasks = []
    results = []
    async with ClientSession(cookies=cookies) as session:
        for country in countries:
            with open(f'players/players_u21_{country}.txt', 'r+') as players_file:
                for player_id in players_file.readlines():
                    player_id = int(player_id)

                    tasks.append(get_skill_player_brazuca(session, player_id, sema))
        responses = await asyncio.gather(*tasks)
        results.append({'player_id': player_id})

    with open('players_skill_u21.csv', 'w') as csv_file:
        fieldnames = responses[0].keys()
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        writer.writeheader()
        for result in responses:
            writer.writerow(result)


if __name__ == '__main__':
    asyncio.run(run())
    # asyncio.run(run_stage2())
