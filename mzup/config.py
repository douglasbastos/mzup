import os


MZ_DOMAIN = 'https://www.managerzone.com'
MY_TEAM_ID = os.getenv('MY_TEAM_ID')

MZ_URL_TEAM_PLAYER_LIST = f"{MZ_DOMAIN}/xml/team_playerlist.php?sport_id=1&team_id={{}}"
MZ_URL_TRAINING_HISTORY = f'{MZ_DOMAIN}/ajax.php?p=trainingGraph&sub=getJsonTrainingHistory&sport=soccer&player_id={{}}'
MZ_URL_SCOUT_REPORT = f'{MZ_DOMAIN}/ajax.php?p=players&sub=scout_report&pid={{}}&sport=soccer'

CHROME_PATH = os.getenv('CHROME_PATH')
USE_CHROME_HEADLESS = os.getenv('USE_CHROME_HEADLESS', '1') == '1'

MONGO_URI = os.getenv('MONGO_URI', 'mongodb://127.0.0.1:27017')

SENTRY_URL = os.getenv('SENTRY_URL')

MZ_USER = os.getenv('MZ_USER')
MZ_PASSWORD = os.getenv('MZ_PASSWORD')


KEY_SKILL_MAP = {
    1: 'speed',
    2: 'stamina',
    3: 'intelligence',
    4: 'passing',
    5: 'shooting',
    6: 'heading',
    7: 'keeping',
    8: 'ball_control',
    9: 'tackling',
    10: 'aerial_passing',
    11: 'set_plays',
}

TELEGRAM_CHAT_ID = os.getenv('TELEGRAM_CHAT_ID')
TELEGRAM_TOKEN = os.getenv('TELEGRAM_TOKEN')

# TODO: MOVER PARA CONSTS
SCHEMA_MESSAGE_MATCHES_INFO = """
*Competição: {category} | {competition_name} | {competition_type}*
Data: {date}
Partida: {home_team_name} x {away_team_name}
Valor da Equipe: {home_team_value} - {away_team_value}
"""