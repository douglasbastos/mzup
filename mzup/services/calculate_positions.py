class Wings():

    def goleiro(self, row):
        result = 0
        result += row['speed'] * 0
        result += row['stamina'] * 0.1
        result += row['intelligence'] * 0.15
        result += row['passing'] * 0
        result += row['shooting'] * 0
        result += row['heading'] * 0
        result += row['keeping'] * 0.6
        result += row['ball_control'] * 0.05
        result += row['tackling'] * 0
        result += row['aerial_passing'] * 0.1
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    def ponta(self, row):
        result = 0
        result += row['speed'] * 0.2
        result += row['stamina'] * 0.15
        result += row['intelligence'] * 0.15
        result += row['passing'] * 0
        result += row['shooting'] * 0
        result += row['heading'] * 0
        result += row['keeping'] * 0
        result += row['ball_control'] * 0.12
        result += row['tackling'] * 0.03
        result += row['aerial_passing'] * 0.35
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    def lateral(self, row):
        result = 0
        result += row['speed'] * 0.23
        result += row['stamina'] * 0.15
        result += row['intelligence'] * 0.15
        result += row['passing'] * 0.02
        result += row['shooting'] * 0
        result += row['heading'] * 0
        result += row['keeping'] * 0
        result += row['ball_control'] * 0.06
        result += row['tackling'] * 0.32
        result += row['aerial_passing'] * 0.07
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    def zagueiro(self, row):
        result = 0
        result += row['speed'] * 0.2
        result += row['stamina'] * 0.2
        result += row['intelligence'] * 0.14
        result += row['passing'] * 0.03
        result += row['shooting'] * 0
        result += row['heading'] * 0.0
        result += row['keeping'] * 0
        result += row['ball_control'] * 0.04
        result += row['tackling'] * 0.32
        result += row['aerial_passing'] * 0.07
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    def volante(self, row):
        result = 0
        result += row['speed'] * 0.05
        result += row['stamina'] * 0.15
        result += row['intelligence'] * 0.17
        result += row['passing'] * 0.05
        result += row['shooting'] * 0
        result += row['heading'] * 0
        result += row['keeping'] * 0
        result += row['ball_control'] * 0.16
        result += row['tackling'] * 0.26
        result += row['aerial_passing'] * 0.16
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    def meia(self, row):
        result = 0
        result += row['speed'] * 0.05
        result += row['stamina'] * 0.15
        result += row['intelligence'] * 0.2
        result += row['passing'] * 0.07
        result += row['shooting'] * 0.02
        result += row['heading'] * 0
        result += row['keeping'] * 0
        result += row['ball_control'] * 0.19
        result += row['tackling'] * 0.12
        result += row['aerial_passing'] * 0.2
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    def cabeceador(self, row):
        result = 0
        result += row['speed'] * 0.05
        result += row['stamina'] * 0.15
        result += row['intelligence'] * 0.15
        result += row['passing'] * 0
        result += row['shooting'] * 0.25
        result += row['heading'] * 0.3
        result += row['keeping'] * 0
        result += row['ball_control'] * 0.1
        result += row['tackling'] * 0
        result += row['aerial_passing'] * 0
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    def atacante(self, row):
        result = 0
        result += row['speed'] * 0.2
        result += row['stamina'] * 0.15
        result += row['intelligence'] * 0.17
        result += row['passing'] * 0
        result += row['shooting'] * 0.31
        result += row['heading'] * 0
        result += row['keeping'] * 0
        result += row['ball_control'] * 0.17
        result += row['tackling'] * 0
        result += row['aerial_passing'] * 0
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)


class Short():

    def goleiro(self, row):
        result = 0
        result += row['speed'] * 0
        result += row['stamina'] * 0.1
        result += row['intelligence'] * 0.15
        result += row['passing'] * 0
        result += row['shooting'] * 0
        result += row['heading'] * 0
        result += row['keeping'] * 0.6
        result += row['ball_control'] * 0.05
        result += row['tackling'] * 0
        result += row['aerial_passing'] * 0.1
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    def lateral(self, row):
        result = 0
        result += row['speed'] * 0.22
        result += row['stamina'] * 0.19
        result += row['intelligence'] * 0.14
        result += row['passing'] * 0.06
        result += row['shooting'] * 0
        result += row['heading'] * 0
        result += row['keeping'] * 0
        result += row['ball_control'] * 0.06
        result += row['tackling'] * 0.3
        result += row['aerial_passing'] * 0.03
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    def zagueiro(self, row):
        result = 0
        result += row['speed'] * 0.2
        result += row['stamina'] * 0.2
        result += row['intelligence'] * 0.14
        result += row['passing'] * 0.07
        result += row['shooting'] * 0
        result += row['heading'] * 0
        result += row['keeping'] * 0
        result += row['ball_control'] * 0.04
        result += row['tackling'] * 0.33
        result += row['aerial_passing'] * 0.03
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    def volante(self, row):
        result = 0
        result += row['speed'] * 0.05
        result += row['stamina'] * 0.14
        result += row['intelligence'] * 0.20
        result += row['passing'] * 0.15
        result += row['shooting'] * 0
        result += row['heading'] * 0
        result += row['keeping'] * 0
        result += row['ball_control'] * 0.15
        result += row['tackling'] * 0.25
        result += row['aerial_passing'] * 0.06
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    def meia(self, row):
        result = 0
        result += row['speed'] * 0.05
        result += row['stamina'] * 0.15
        result += row['intelligence'] * 0.19
        result += row['passing'] * 0.18
        result += row['shooting'] * 0.08
        result += row['heading'] * 0
        result += row['keeping'] * 0
        result += row['ball_control'] * 0.19
        result += row['tackling'] * 0.12
        result += row['aerial_passing'] * 0.04
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)

    def atacante(self, row):
        result = 0
        result += row['speed'] * 0.2
        result += row['stamina'] * 0.15
        result += row['intelligence'] * 0.17
        result += row['passing'] * 0
        result += row['shooting'] * 0.31
        result += row['heading'] * 0
        result += row['keeping'] * 0
        result += row['ball_control'] * 0.17
        result += row['tackling'] * 0
        result += row['aerial_passing'] * 0
        result += row['set_plays'] * 0
        result += row['experience'] * 0.05
        result += row['form'] * 0.05

        result = result * 100 / 110
        return round(result * 10, 1)