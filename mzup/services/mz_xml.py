import asyncio

from aiohttp import ClientSession

from mzup.config import MZ_URL_TEAM_PLAYER_LIST
from mzup.services.log import logger
from mzup.services.request import fetch


async def get_team_player_list(session: ClientSession, team_id: int) -> tuple:
    url = MZ_URL_TEAM_PLAYER_LIST.format(team_id)
    logger.info(f'Collect info players: {team_id}')
    sema = asyncio.Semaphore(20)

    return await fetch(url.format(team_id), session, sema)
