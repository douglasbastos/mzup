import asyncio

async def fetch2(url, session):
    async with session.get(url) as response:
        print(url)
        return await response.read()


async def fetch(url, session, sema):
    async with sema, session.get(url) as response:
        print(url)
        return await response.read()


async def fetch_post(url, session, data, sema):
    async with sema, session.post(url, data=data) as response:
        return await response.read()
