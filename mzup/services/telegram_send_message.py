import telegram

from mzup import config
from mzup.services.log import logger


def send_message(msg, parse_mode=None):
    """
    Send a mensage to a telegram user specified on chatId
    chat_id must be a number!
    """
    chat_id = config.TELEGRAM_CHAT_ID
    token = config.TELEGRAM_TOKEN

    bot = telegram.Bot(token=token)
    logger.info(f'Enviando mensagem pelo telegram: {msg}')
    bot.sendMessage(chat_id=chat_id, text=msg, parse_mode=parse_mode)
