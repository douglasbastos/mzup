from http import HTTPStatus
from time import sleep

import requests
import xmltodict
from parsel import Selector

from mzup import config
from mzup.config import MZ_DOMAIN
from mzup.services.log import logger


def go_to_fixtures_page(driver):
    driver.get(f'{MZ_DOMAIN}/?p=match&sub=scheduled')
    logger.info('Accessing the fixtures page')
    sleep(3)


def get_info_fixture(source_code, category):
    source_fixtures = source_code.xpath("//dl[@id='fixtures-results-list']/dd[contains(@class, 'odd')]").extract()
    all_matches = {}

    for source in source_fixtures:
        source = Selector(text=source)


        competition_type = source.xpath("//dd[@class='responsive-hide match-reference-text-wrapper flex-grow-0']/span/text()").extract_first()
        competition_name = source.xpath("//dd[@class='responsive-hide match-reference-text-wrapper flex-grow-0']/a/text()").extract_first()
        match_id = source.xpath("//dd[@class='bold score-cell-wrapper textCenter flex-grow-0']/a/@href").extract_first()

        match_id = match_id.replace('/?p=match&sub=result&mid=', '').split('&tid')[0]
        all_matches[match_id] = {
            'competition_name': competition_name,
            'competition_type': competition_type,
            'category': category
        }

    return all_matches

def get_fixtures_by_xml():
    logger.info('Coletando informações de partidas via XML')
    resp = requests.get(
        f'{MZ_DOMAIN}/xml/team_matchlist.php?sport_id=1&'
        f'team_id={config.MY_TEAM_ID}&match_status=2&limit=20'
    )
    if resp.status_code == HTTPStatus.OK:
        matches = xmltodict.parse(resp.text)['ManagerZone_MatchList']['Match']

        all_matches = {}
        for match in matches:

            all_matches[match['@id']] = {
                'match_id': match['@id'],
                'date': match['@date'],
                'home_team_name': match['Team'][0]['@teamName'],
                'home_team_id': match['Team'][0]['@teamId'],
                'away_team_name': match['Team'][1]['@teamName'],
                'away_team_id': match['Team'][1]['@teamId']
            }

        return all_matches

def get_fixtures_by_category(driver):
    categories = {
        'No age restriction': 'SENIORS',
        'Abaixo 18': 'U18',
        'Abaixo 21': 'U21',
        'Abaixo 23': 'U23'
    }
    all_matches = {}
    for category, category_initials in categories.items():
        el = driver.find_element_by_xpath("//select[@name='selectType']")
        el.send_keys(category)

        all_matches = {
            **get_info_fixture(source_code=Selector(text=driver.page_source), category=category_initials),
            **all_matches
        }

    return all_matches


def get_fixtures(type: str, driver=None):
    if type == 'xml':
        fixtures = get_fixtures_by_xml()
    elif type == 'browser':
        if not driver:
            raise AttributeError('Argumento driver não pode ser None')

        fixtures = get_fixtures_by_category(driver)
    else:
        raise AttributeError('Argumento type deve ser xml ou browser')

    return fixtures
