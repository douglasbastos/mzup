import re
from time import sleep
from typing import Iterator

from parsel import Selector
from selenium.common.exceptions import NoSuchElementException

from mzup.commands.collect_scout_report import get_collect_scout_report
from mzup.config import MZ_DOMAIN
from mzup.expections import PlayersTransferListEnded
from mzup.services import mongodb
from mzup.services.log import logger
from mzup.services.managerzone.get_player_maximizations import \
    get_player_maximizations
from mzup.services.managerzone.get_session_id import get_session_id
from mzup.services.managerzone.parser_skill import parser_player_market


def go_to_transfer_market_page(driver):
    driver.get(f'{MZ_DOMAIN}/?p=transfer')
    logger.info('Accessing the transfer market page')
    sleep(5)


def collect_skills(sel):
    name = sel.xpath(
        "//div[@id='players_container']//span[@class='player_name']/text()").extract()
    player_id = sel.xpath(
        "//div[@id='players_container']//span[@class='player_id_span']/text()").extract()
    age = sel.xpath(
        "//table/tbody/tr[1]/td[2]/table/tbody/tr[1]/td[2]/strong/text()").extract()
    country = sel.xpath("//a[@class='subheader']/img/@title").extract()
    nationality = sel.xpath("//a[@class='subheader']/img/@src").extract()
    value = sel.xpath(
        "//table/tbody/tr[1]/td/table/tbody/tr/td[2]/span[@class='bold']/text()").extract()
    salary = sel.xpath(
        "//table/tbody/tr[1]/td/table/tbody/tr/td[3]/span[@class='bold']/text()").extract()
    total_balls = sel.xpath(
        "//table/tbody/tr[1]/td[2]/table/tbody/tr[6]/td[2]/strong/text()").extract()
    speed = sel.xpath("//tr[1]/td[@class='skillval']/text()").extract()
    stamina = sel.xpath("//tr[2]/td[@class='skillval']/text()").extract()
    intelligence = sel.xpath("//tr[3]/td[@class='skillval']/text()").extract()
    passing = sel.xpath("//tr[4]/td[@class='skillval']/text()").extract()
    shooting = sel.xpath("//tr[5]/td[@class='skillval']/text()").extract()
    heading = sel.xpath("//tr[6]/td[@class='skillval']/text()").extract()
    keeping = sel.xpath("//tr[7]/td[@class='skillval']/text()").extract()
    ball_control = sel.xpath("//tr[8]/td[@class='skillval']/text()").extract()
    tackling = sel.xpath("//tr[9]/td[@class='skillval']/text()").extract()
    aerial_passing = sel.xpath(
        "//tr[10]/td[@class='skillval']/text()").extract()
    set_plays = sel.xpath("//tr[11]/td[@class='skillval']/text()").extract()
    experience = sel.xpath("//tr[12]/td[@class='skillval']/text()").extract()
    form = sel.xpath("//tr[13]/td[@class='skillval']/text()").extract()

    name_selling_club = sel.xpath(
        "//table/tbody/tr[2]/td[2]/strong[@class='clippable']/a/text()").extract()
    id_selling_club = sel.xpath(
        "//table/tbody/tr[2]/td[2]/strong[@class='clippable']/a/@href").extract()
    asking_price = sel.xpath(
        "//div/div[@class='box_dark'][1]/table/tbody/tr[4]/td[2]/strong/text()").extract()
    deadline = sel.xpath(
        "//div/div[@class='box_dark'][1]/table/tbody/tr[3]/td[2]/strong/text()").extract()

    index = 0
    while True:
        try:
            yield player_id[index], {
                'salary': salary[index],
                'name': name[index],
                'player_id': player_id[index],
                'age': age[index],
                'country': country[index],
                'nationality': nationality[index],
                'value': value[index],
                'total_balls': total_balls[index],
                'speed': speed[index],
                'stamina': stamina[index],
                'intelligence': intelligence[index],
                'passing': passing[index],
                'shooting': shooting[index],
                'heading': heading[index],
                'keeping': keeping[index],
                'ball_control': ball_control[index],
                'tackling': tackling[index],
                'aerial_passing': aerial_passing[index],
                'set_plays': set_plays[index],
                'experience': experience[index],
                'form': form[index],
                'name_selling_club': name_selling_club[index],
                'id_selling_club': id_selling_club[index],
                'asking_price': asking_price[index],
                'deadline': deadline[index]
            }
        except IndexError:
            logger.info('Finished page players')
            break
        else:
            index += 1


def go_to_next_page(driver, current_page: int):
    """
    Solicita ao browser para ir para a próxima página na lista de transferência
    """
    next_page = current_page + 1
    logger.info(f'Going to page: {next_page}')
    try:
        driver.find_element_by_xpath(
            f"//div[@class='transferSearchPages textCenter top']/a[text()={next_page}]").click()
    except NoSuchElementException:
        logger.info('Number of players in the transfer list has ended')
        raise PlayersTransferListEnded


async def management_market_players(sel: Selector, session_mz_id: str,
                                    player_ids: set, players_ids_with_scout: Iterator) -> Iterator:
    responses_maximization = await get_player_maximizations(
        players_id=player_ids,
        session_mz_id=session_mz_id
    )
    responses_scout_report = await get_collect_scout_report(
        player_ids=player_ids,
        session_mz_id=session_mz_id
    )
    result = []
    players = dict(collect_skills(sel))
    for player_id, player in players.items():
        if player_ids.intersection([player_id]):
            player.update(responses_maximization[player_id])
            player.update(responses_scout_report.get(player_id, {}))
            result.append(parser_player_market(player))
    return result

def get_player_ids_with_scout_report(player_ids, players_born):
    for player_id, player_born in zip(player_ids, players_born):
        born = int(re.sub('[^0-9]', '', player_born))
        if born >= 52:
            yield player_id


async def get_transfers_players(driver) -> list:
    """
    Coleta jogadores que estão no mercado de transferência
    """
    go_to_transfer_market_page(driver)
    session_mz_id = get_session_id(driver)
    players_collect = {i['player_id'] for i in mongodb.players_id_collected_transfer_market()}

    logger.info('Capturing players market data')
    players_result = []
    while True:
        sel = Selector(text=driver.page_source)
        player_ids = sel.xpath("//div[@id='players_container']//span[@class='player_id_span']/text()").extract()
        players_born = sel.xpath("//table/tbody/tr[1]/td[2]/table/tbody/tr[2]/td[2]/strong/text()").extract()
        player_ids = set(player_ids) - players_collect
        player_ids_with_scout_report = get_player_ids_with_scout_report(player_ids, players_born)
        current_page = sel.xpath("//div[@class='transferSearchPages textCenter top']/b/text()").extract_first()
        try:
            go_to_next_page(driver, current_page=int(current_page))
            sleep(10)
        except PlayersTransferListEnded:
            players_result += await management_market_players(
                sel, session_mz_id, player_ids, player_ids_with_scout_report
            )
            break
        else:
            players_result += await management_market_players(
                sel, session_mz_id, player_ids, player_ids_with_scout_report
            )

    return players_result
