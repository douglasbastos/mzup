import asyncio
import json
from typing import Iterable

from aiohttp import ClientSession

from mzup.config import KEY_SKILL_MAP, MZ_URL_TRAINING_HISTORY
from mzup.services.log import logger
from mzup.services.request import fetch


def parser_player_maximization(body: str) -> Iterable:
    for line in body.split('var'):
        text_match = ' series = '
        if line.startswith(text_match):
            data = json.loads(line.replace(text_match, '').replace(';', ''))
            break
    else:
        return []

    for item in data:
        if item.get('showInNavigator') == 'false' and item['color'] == 'rgba(255,0,0,0.7)':
            code = item['data'][0]['y']
            yield KEY_SKILL_MAP[code]


async def request_training_history(session: ClientSession, player_id: int) -> tuple:
    url = MZ_URL_TRAINING_HISTORY.format(player_id)
    logger.info(f'Getting player_id maximization: {player_id}')
    return await fetch(url, session=session)


async def get_player_maximizations(players_id: Iterable, session_mz_id: str) -> dict:
    cookies = {'PHPSESSID': session_mz_id}
    async with ClientSession(cookies=cookies) as session:
        tasks = []
        for player_id in players_id:
            tasks.append(request_training_history(session, player_id))

        responses = await asyncio.gather(*tasks)
        players_result = {}
        for player_id, response in zip(players_id, responses):
            body = response.decode()

            # Quando body está vazio significa que o relatório não está liberado
            if body:
                maximizations = list(parser_player_maximization(body))
                players_result[player_id] = {
                    'is_visible_training_reports': maximizations is not None,
                    'maximizations': maximizations
                }
            else:
                players_result[player_id] = {}

        return players_result
