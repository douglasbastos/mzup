import re
from datetime import datetime

from mzup.services.log import logger


def parser_player_market(player: dict):
    logger.info(f'Parsing players {player["player_id"]} skills')
    def get_number(value: str) -> int:
        return int(re.sub('[^0-9]', '', value))

    player['salary'] = get_number(player['salary'])
    player['value'] = get_number(player['value'])
    player['experience'] = get_number(player['experience'])
    player['form'] = get_number(player['form'])
    player['total_balls'] = get_number(player['total_balls'])
    player['age'] = get_number(player['age'])
    player['speed'] = get_number(player['speed'])
    player['stamina'] = get_number(player['stamina'])
    player['intelligence'] = get_number(player['intelligence'])
    player['passing'] = get_number(player['passing'])
    player['shooting'] = get_number(player['shooting'])
    player['heading'] = get_number(player['heading'])
    player['keeping'] = get_number(player['keeping'])
    player['ball_control'] = get_number(player['ball_control'])
    player['tackling'] = get_number(player['tackling'])
    player['aerial_passing'] = get_number(player['aerial_passing'])
    player['set_plays'] = get_number(player['set_plays'])
    player['id_selling_club'] = get_number(player['id_selling_club'])
    player['asking_price'] = get_number(player['asking_price'])
    player['deadline'] = datetime.strptime(player['deadline'], '%d/%m/%Y %I:%M%p')
    player['nationality'] = player['nationality'].split('/')[-1].replace('.png', '')

    return player
