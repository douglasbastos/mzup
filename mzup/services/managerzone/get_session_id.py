import os


def get_session_id(driver) -> str:
    """
    Coleta o session_id de um usuário logado
    """
    for search in driver.get_cookies():
        if search['name'] == 'PHPSESSID':
            return search['value']
    else:
        raise Exception
