from time import sleep

from mzup.config import MZ_DOMAIN, MZ_USER, MZ_PASSWORD
from mzup.services.log import logger


def login(driver):
    """
    Realiza login no managerzone
    """
    import os

    driver.get(MZ_DOMAIN)

    logger.info('Acessing the managerzone login page')
    username = driver.find_element_by_id("login_username")
    password = driver.find_element_by_id("login_password")
    username.send_keys(MZ_USER)
    password.send_keys(MZ_PASSWORD)

    driver.find_element_by_css_selector(".floatLeft .buttonClassMiddle").click()
    logger.info('Logging...')
    sleep(3)

    logger.info('Logged')
