import logging


def get_logger():
    format = '{"%(levelname)s":"%(asctime)s", "%(funcName)s":%(lineno)d, "%(threadName)s":"%(message)s"}'
    logging.basicConfig(format=format)
    logger = logging.getLogger('Crawler Mz')
    logger.setLevel(logging.DEBUG)
    return logger


logger = get_logger()
