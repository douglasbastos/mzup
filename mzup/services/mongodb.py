from pymongo import MongoClient

from mzup.config import MONGO_URI
from mzup.services.log import logger

mongo_client = MongoClient(MONGO_URI)
db = mongo_client['mz']


    