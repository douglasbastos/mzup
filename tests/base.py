import asyncio
import json
import os
from unittest import TestCase


CURRENT_DIR = os.path.dirname(__file__)
FIXTURES_PATH = os.path.join(CURRENT_DIR, 'fixtures')


class AsyncTestCase(TestCase):

    # noinspection PyPep8Naming
    def __init__(self, methodName='runTest', loop=None):
        self.loop = loop or asyncio.get_event_loop()
        self._function_cache = {}
        super(AsyncTestCase, self).__init__(methodName=methodName)

    def coroutine_function_decorator(self, func):
        def wrapper(*args, **kw):
            return self.loop.run_until_complete(func(*args, **kw))
        return wrapper

    def __getattribute__(self, item):
        attr = object.__getattribute__(self, item)
        if asyncio.iscoroutinefunction(attr):
            if item not in self._function_cache:
                self._function_cache[item] = self.coroutine_function_decorator(attr)
            return self._function_cache[item]
        return attr


def get_fixture(file_name: str) -> dict or str:
    file_type = file_name.split('.')[-1]
    with open(os.path.join(FIXTURES_PATH, file_name), encoding='utf8') as fp:
        if file_type == 'json':
            return json.load(fp)
        else:
            return fp.read()


def with_fixture(fixture_path):
    def wrapper(func):
        def decorator(self):
            fixture = get_fixture(fixture_path)
            return func(self, fixture)
        return decorator
    return wrapper
