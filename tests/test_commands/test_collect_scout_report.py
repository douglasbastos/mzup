import unittest
from unittest import mock

import responses

from mzup.commands import collect_scout_report
from mzup.config import MZ_DOMAIN
from tests.utils import get_fixture


class CollectScoutReportTests(unittest.TestCase):
    def setUp(self) -> None:
        self.player = 123
        self.url = f'{MZ_DOMAIN}/ajax.php?p=players&sub=scout_report&pid={self.player}&sport=soccer'

    @responses.activate
    def test_first(self):
        body = get_fixture('scout_report/main.html')
        responses.add(
            method=responses.GET,
            url=self.url,
            status=200,
            body=body
        )

        result = collect_scout_report.run(player=self.player)
        expected = {
            'highest_potential': ['passing', 'tackling'],
            'highest_stars': 3,
            'lowest_potential': ['set_plays', 'ball_control'],
            'lowest_stars': 1,
            'training_speed_stars': 2
        }
        self.assertEqual(expected, result)
