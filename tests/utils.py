import os


CURRENT_DIR = os.path.dirname(__file__)
FIXTURES_PATH = os.path.join(CURRENT_DIR, 'fixtures')


def get_fixture(file_name: str) -> bytes:
    with open(os.path.join(FIXTURES_PATH, file_name), encoding='utf8') as fp:
        return fp.read().encode()
