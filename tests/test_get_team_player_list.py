import aiohttp

from aioresponses import aioresponses

from mzup.config import MZ_URL_TEAM_PLAYER_LIST
from mzup.services.mz_xml import get_team_player_list
from tests.base import AsyncTestCase, get_fixture


class GetTeamPlayerListTest(AsyncTestCase):
    def setUp(self):
        self.session = aiohttp.ClientSession()

    def tearDown(self):
        self.session.close()

    async def test_get_team_player_list(self):
        team_id = 123

        with aioresponses() as mocked:
            fixture = get_fixture('mz_xml/team_playerlist.xml')
            mocked.get(url=MZ_URL_TEAM_PLAYER_LIST.format(team_id),
                       status=200, body=fixture)
            players = await get_team_player_list(self.session, team_id=team_id)
            self.assertEqual(12, len(players))
            for player_id, player in players.items():
                skills = ['id', 'name', 'shirtNo', 'age', 'birthSeason',
                          'birthDay', 'height', 'weight', 'value', 'salary',
                          'countryShortname', 'injuryType', 'injuryDays',
                          'junior']
                self.assertListEqual(skills, list(player.keys()))
